# Jamf Kace Deploy

An approach for flexible KACE Agent deploys with Jamf

## Prerequisites

- [ ] Latest KACE Agent installer pkg, such as that from `smb://kbox/client` (substituting `kbox` with your KACE server FQDN). Note that the hostname and token strings do not need to be in the filename for this approach.
- [ ] [KACE Agent Token](https://support.quest.com/technical-documents/kace-systems-management-appliance/12.0%20common%20documents/administrator-guide/93) to avoid machines entering KACE quarantine

## 1. Upload your KACE Package

1. Log in to your Jamf Pro instance.
2. Navigate to Settings > Computer Management > Packages
3. Click "New"
4. Click "Choose a file" to upload your KACE installer (e.g., ampagent-12.0.38.macos.all.pkg). The "Display Name" field will auto-fill with your package's filename.
5. Give the package a category.
6. Click "Save." All other defaults are okay as-is.

## 2. Upload your KACE script

1. In Jamf Pro, navigate to Settings > Computer Management > Scripts
2. Click "New"
3. Give the script a display name, such as "Install Kace Agent"
4. Give the script a category.
5. Enter any relevant notes for other admins to see in the future, as desired, in the "Notes" field.
6. Switch to the Script tab and paste the body of [install-kace.sh](/install-kace.sh) into the editor.
7. Switch to the options tab and give Parameters 4 and 5 names. These should align with `$4` and `$5` in the script and be comprehensible to admins configuring a policy that uses this script:
    - __Parameter 4__: `KBOX URL`
    - __Parameter 5__: `KACE Token`
8. Click "Save." The default priority, "After" is acceptable for this policy. No limitations need to be configured here.

## 3. Create KACE Extension Attributes
Because KACE does not install as a standard application, we'll need some custom Extension Attributes to help with grouping and reporting. We use three: KUID, KACE Server, and KACE Agent Status.

We'll create KACE Agent Status here:
1. In Jamf Pro, navigate to Settings > Computer Management > Extension Attributes
2. Click "New"
3. Name the EA "KACE Agent Status" and make sure it is enabled.
4. Set "Data Type" to "String"
5. Select where in the computer inventory this should display, such as the "Extension Attributes" tab
6. Set "Input Type" to "Script"
7. Copy the body of [agent_status.sh](Extension Attributes/agent_status.sh) into the editor
8. Click "Save"
9. Repeat with the other Extension Attribute scripts.

## 4. Create a Smart Group for KACE Install status
1. In Jamf Pro, navigate to Computers > Smart Computer Groups
2. Give it a name, such as `Software - KACE - Installed`
3. In the criteria tab, add the following:
    - `KACE Agent Version` (the name of our EA created above) "is not" `Not installed`
4. Click "Save"

You can create additional groups using the other EAs we created. Consider ones like:
- KACE Server - Configuration Missing (for when amp.conf is not present)
- KACE Server - Unexpected URL (for times when a computer might point to a different KBOX URL unexpectedly)
- Extension Attribute - Reinstall KACE Agent - Requested (a more advanced use case in which you create a toggle in a computer record for a technician to indicate if they need KACE reinstalled manually)

## 5. Build a deployment policy

Our goal in designing our core policy is to make something flexible and transparent to future admins.

1. In Jamf Pro, navigate to Computers > Policies
2. Click "New"
3. Give your policy a concise, but comprehensible name.
4. Assign the policy a category.
5. Select a trigger and frequency for the policy. Your choice may depend on how you'd like this to be deployed:
    - Using "Custom" with "Ongoing" frequency is very powerful (and is how we use this policy). By setting an ongoing manual trigger, you can run this policy in a number of flexible ways:
        - As part of an onboarding experience with `sudo jamf policy -event customTrigger` in a controller script, such as [DEPNotify](https://gitlab.com/Mactroll/DEPNotify), [swiftDialog](https://github.com/bartreardon/swiftDialog/), [Octory](https://www.octory.io/), etc.
        - As a reusable "building block" nested in other policies. Useful for scenarios like:
            - Reinstalling the KACE agent after a crafty user uninstalls it.
            - Triggering Jamf to reinstall KACE at next check-in. A good troubleshooting tool when combined with Extension Attributes for automatic repair.
    - Using "Enrollment Complete" to automatically install KACE when the computer finishes enrolling with Jamf (either via ADE or manual), or when running `sudo jamf policy -event enrollmentComplete`. Using this with "Once per computer" would mean that it would only runs once on a computer after enrollment. Any subsequent runs of this policy would require that computer's entry in the policy log to be flushed to allow it back into scope. You could also use "Ongoing."
    - etc...
6. Click the "Packages" payload tab
7. Add your upload ampagent installer pkg
8. Set the action to "Cache"
9. Click the "Scripts" payload tab
10. Add your KACE installer script.
11. Ensure that "Priority" is "After," which should trigger the script after the package is cached on the machine
12. Define your KACE URL in the first parameter. This is written as, say, `kace.org.edu` (i.e., no protocol prefixes, slashes, etc.)
13. Paste your token into the KACE Token parameter field.
14. Click the "Maintenance" payload and configure it. This should default to run an inventory update with all other options disabled, which is what we want.
15. Click the "Scope" tab and define as needed. For example:
    - __Targets__: Whichever group of computers you'd like to include. This might be all computers, or otherwise.
    - __Limitations__: Likely not necessary for this policy.
    - __Exclusions__: Global Exclusions, other groups as needed
        - We personally do not exclude "Software - KACE - Installed" in our core policy because we use it as a manually triggered, reusable building block. We do, however, use this in other policies that run on automatic triggers.
16. Click "Save"
17. Test accordingly on your machines.
