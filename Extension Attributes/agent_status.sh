#!/bin/sh

# Check to see if the KACE agent is installed.
# If the agent is installed, report the agent
# version.

if [[ -f "/Library/Application Support/Quest/KACE/data/version" ]]; then
   RESULT=$(cat "/Library/Application Support/Quest/KACE/data/version" | head -1)
   /bin/echo "<result>$RESULT</result>"
else
   /bin/echo "<result>Not installed</result>"
fi
