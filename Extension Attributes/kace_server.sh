#!/bin/sh

# Check to see if the KACE agent is installed.
# If the agent is installed, report the agent
# version.

if [[ -f "/Library/Application Support/Quest/KACE/data/version" ]]; then
	if [[ -f  "/Library/Application Support/Quest/KACE/data/amp.conf" ]]; then
		RESULT=$(cat "/Library/Application Support/Quest/KACE/data/amp.conf" | grep "host=" | awk -F= '{print $2}')
		/bin/echo "<result>$RESULT</result>"
	else
		/bin/echo "<result>amp.conf not found</result>"
	fi
else
	/bin/echo "<result>Not installed</result>"
fi
