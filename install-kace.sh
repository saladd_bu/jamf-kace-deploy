#!/bin/bash

KBOX="${4}"	# KBOX URL provided by Jamf parameter
KTOKEN="${5}"	# Token for trusted KACE install

KPKG_DIR="/Library/Application Support/JAMF/Waiting Room/"

ResetAmpConf(){
	"/Library/Application Support/Quest/KACE/bin/AMPTools" -resetconf host="${KBOX}" token="${KTOKEN}"

    echo "Running inventory"
    "/Library/Application Support/Quest/KACE/bin/runkbot" 2 0
}

KPKG=$(ls "${KPKG_DIR}" | grep --regexp='^ampagent.*\.pkg$' | tail -1) # Get the latest filename

# Install Kace agent using Jamf parameters
export KACE_SERVER="${KBOX}"
export KACE_TOKEN="${KTOKEN}"
/usr/sbin/installer -pkg "${KPKG_DIR}${KPKG}" -target /

# Cleanup
/bin/rm -rf "${KPKG_DIR}${KPKG}"
/bin/rm -rf "${KPKG_DIR}${KPKG}.cache.xml"

# Get processor type. We use this to correct for a Rosetta installer issue.
# KACE installer currently in use as of 10/2021 is Intel only. When switching to
# Rosetta to install, installer cannot access our KBOX hostname parameter.
# If an Apple processor is found, attempt to reconfigure agent to point to
# our specified hostname instead of default KBOX.
processor=$(/usr/sbin/sysctl -n machdep.cpu.brand_string | grep -o "Apple")

if [[ "$processor" ]]; then
    echo "Apple processor installed. Resetting amp.conf."

    ResetAmpConf
fi

# Check for amp.conf presence and reset if not there
if [[ ! -e  "/Library/Application Support/Quest/KACE/data/amp.conf" ]]; then
	echo "Could not find amp.conf. Triggering a reset."

	ResetAmpConf
fi

